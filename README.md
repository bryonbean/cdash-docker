# Docker files for CDash 

In the instructions below, we will assume CDash is installed at /path/to/projects/CDash.

Copy files to your CDash root:
```
CDASH_ROOT=/path/to/projects/CDash

cd /path/to/projects/
git clone https://gitlab.kitware.com/bryonbean/cdash-docker.git
cd cdash-docker

cp docker-compose.yml $CDASH_ROOT
cp docker-entrypoint.sh $CDASH_ROOT
cp docker-exectests.sh $CDASH_ROOT
cp Dockerfile $CDASH_ROOT
```
Run tests:
```
docker-compose up --build
docker exec cdash-host ./docker-exectests.sh
docker-compose down
```

## Troubleshooting
If you are trying to run tests and they are all failing try:
- ensure that the docker server (cdash-host) is able to write (overwrite) the files in your `public/rss` directory
